from flask import Flask, jsonify, render_template, url_for
from flask import abort
# import config
from flask import redirect
from flask import make_response
from flask_bootstrap import Bootstrap
from flask import request

app = Flask(__name__)
bootstrap = Bootstrap(app)


# app.config.from_object(config)

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]


@app.route('/todo/api/v1.0/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})


@app.route('/hello')
@app.route('/hello/<name>')
def hello(name=None):
    app.logger.debug('A value for debugging %s', name)
    app.logger.debug('name= ' + name)
    return render_template('hello.html', name=name)


@app.route('/')
def hello_world():
    return redirect(url_for('login'))
    # return '<h1>Hello World!<h1>'
    # return '<h1> Bad Request</h1>', 400


@app.route('/user/<name>')
def user(name):
    return '<h1>Hello, %s!<h1>' % name


# @app.route('/index')
# def index():
#     user_agent = request.headers.get('User-Agent')
#     return '<p>Your browser is %s<p>' % user_agent


@app.route('/index')
def index():
    response = make_response('<h1>This document carries a cookie!</h1>')
    response.set_cookie('answer', '42')
    return response


# @app.route('/index')
# def index():
#     return redirect('http://www.baidu.com')


@app.errorhandler(404)
def not_found(e):
    return render_template('404.html'), 404


@app.route('/user/<int:id>')
def user_id(id):
    return '<h1>Hello, %d!<h1>' % id


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return jsonify({'tasks': tasks})
    else:
        return render_template('login.html')


# def load_user(id):
#     if id == 123456:
#         return 'hahaha'
#     return ''


# @app.route('user/<id>')
# def get_user(id):
#     user = load_user(id)
#     if not user:
#         abort(404)
#     return '<h1>Hello, %s</h1>' % user.name


if __name__ == '__main__':
    app.debug = True
    app.run()
