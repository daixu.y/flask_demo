from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/user/<name>')
def user(name):
    return render_template('user.html', name=name)


# @app.route('/')
# def hello_world():
#     return '<h1>Hello World!<h1>'


if __name__ == '__main__':
    # app.debug = True
    app.run()
